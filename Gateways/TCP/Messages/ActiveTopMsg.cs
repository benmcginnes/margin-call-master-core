﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RocketCore.Gateways.TCP.Messages
{
    class ActiveTopMsg : IJsonSerializable
    {
        private bool side;
        private List<OrderBuf> act_top;
        private DateTime dt_made;

        internal ActiveTopMsg(bool side, List<OrderBuf> act_top, DateTime dt_made) //конструктор сообщения
        {
            this.side = side;
            this.act_top = act_top;
            this.dt_made = dt_made;
        }

        public string Serialize()
        {
            if (side) //sell
            {
                return JsonManager.FormTechJson((int)MessageTypes.NewActiveSellTop, act_top, dt_made);
            }
            else //buy
            {
                return JsonManager.FormTechJson((int)MessageTypes.NewActiveBuyTop, act_top, dt_made);
            }
        }
    }
}
