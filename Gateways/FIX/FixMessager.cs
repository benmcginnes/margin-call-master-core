﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickFix;
using QuickFix.Fields;

namespace RocketCore
{
    static class FixMessager
    {
        #region FIX MESSAGE QUEUEING

        internal static void NewMarketDataIncrementalRefresh(Trade trade) //new market data incremental refresh
        {
            QuickFix.FIX44.MarketDataIncrementalRefresh mdir = new QuickFix.FIX44.MarketDataIncrementalRefresh();
            mdir.Set(new NoMDEntries(1)); //TODO

            QuickFix.FIX44.MarketDataIncrementalRefresh.NoMDEntriesGroup group = new QuickFix.FIX44.MarketDataIncrementalRefresh.NoMDEntriesGroup();
            group.Set(new MDUpdateAction(MDUpdateAction.NEW));
            group.Set(new MDEntryType(MDEntryType.TRADE));
            group.Set(new MDEntryID(trade.TradeId.ToString()));
            group.Set(new Symbol("btc_eur"));
            //TODO добавить SIDE в QuickFix
            group.Set(new MDEntryPx(trade.Rate));
            group.Set(new MDEntrySize(trade.Amount));
            group.Set(new MDEntryTime(trade.DtMade));

            mdir.AddGroup(group);

            Queues.fix_trades_queue.Enqueue(mdir);            
        }

        internal static void NewMarketDataIncrementalRefresh(bool order_kind, Order order) //new market data incremental refresh
        {
            QuickFix.FIX44.MarketDataIncrementalRefresh mdir = new QuickFix.FIX44.MarketDataIncrementalRefresh();
            mdir.Set(new NoMDEntries(1)); //TODO

            QuickFix.FIX44.MarketDataIncrementalRefresh.NoMDEntriesGroup group = new QuickFix.FIX44.MarketDataIncrementalRefresh.NoMDEntriesGroup();
            group.Set(new MDUpdateAction(MDUpdateAction.NEW));
            char mdet = order_kind ? MDEntryType.OFFER : MDEntryType.BID;
            group.Set(new MDEntryType(mdet));
            group.Set(new MDEntryID(order.OrderId.ToString()));
            group.Set(new Symbol("btc_eur"));
            group.Set(new MDEntryPx(order.Rate));
            group.Set(new MDEntrySize(order.ActualAmount));
            group.Set(new MDEntryTime(order.DtMade));

            mdir.AddGroup(group);

            Queues.fix_orders_queue.Enqueue(mdir);
        }

        internal static void NewExecutionReport(string external_data, long func_call_id, bool order_kind, Order order) //new execution report => FIX client
        {
            string[] fix_vals = external_data.Split('&');
            if (fix_vals.Length == 2)
            {
                ConcurrentQueue<Message> queue;
                if (Queues.fix_dict.TryGetValue(fix_vals[0], out queue))
                {
                    queue.Enqueue(FormExecReport(fix_vals[1], func_call_id, order_kind, order));
                }
            }
        }

        internal static void NewExecutionReport(string external_data, bool order_kind, Order order, Trade trade) //new execution report => FIX client
        {
            string[] fix_vals = external_data.Split('&');
            if (fix_vals.Length == 2)
            {
                ConcurrentQueue<Message> queue;
                if (Queues.fix_dict.TryGetValue(fix_vals[0], out queue))
                {
                    queue.Enqueue(FormExecReport(fix_vals[1], order_kind, order, trade));
                }
            }
        }

        internal static void NewExecutionReport(string sender_comp_id, StatusCodes err_code, long func_call_id, QuickFix.FIX44.NewOrderSingle ord, Order order) //new order rejection execution report => FIX client
        {
            ConcurrentQueue<Message> queue;
            if (Queues.fix_dict.TryGetValue(sender_comp_id, out queue))
            {
                queue.Enqueue(FormExecReport(err_code, func_call_id, ord, order));
            }
        }
        
        internal static void NewExecutionReport(string sender_comp_id, StatusCodes status_code, QuickFix.FIX44.OrderStatusRequest stareq, bool order_kind, Order order) //new status execution report => FIX client
        {
            ConcurrentQueue<Message> queue;
            if (Queues.fix_dict.TryGetValue(sender_comp_id, out queue))
            {
                queue.Enqueue(FormExecReport(status_code, stareq, order_kind, order));
            }
        }

        internal static void NewExecutionReport(string sender_comp_id, long func_call_id, QuickFix.FIX44.OrderCancelRequest canreq, bool order_kind, Order order) //new cancel execution report => FIX client
        {
            ConcurrentQueue<Message> queue;
            if (Queues.fix_dict.TryGetValue(sender_comp_id, out queue))
            {
                queue.Enqueue(FormExecReport(func_call_id, canreq, order_kind, order));
            }
        }

        internal static void NewOrderCancelReject(string sender_comp_id, StatusCodes err_code, long func_call_id, QuickFix.FIX44.OrderCancelRequest canreq) //new cancel reject => FIX client
        {
            ConcurrentQueue<Message> queue;
            if (Queues.fix_dict.TryGetValue(sender_comp_id, out queue))
            {
                queue.Enqueue(FormOrdCancReject(err_code, func_call_id, canreq));
            }
        }

        #endregion

        #region EXECUTION REPORT FORMATION

        private static QuickFix.FIX44.ExecutionReport FormExecReport(string cl_ord_id, long func_call_id, bool order_kind, Order order)
        {
            char side = (order_kind) ? Side.SELL : Side.BUY;
            
            QuickFix.FIX44.ExecutionReport exec_report = new QuickFix.FIX44.ExecutionReport(
                new OrderID(order.OrderId.ToString()),
                new ExecID(func_call_id.ToString()),
                new ExecType(ExecType.NEW),
                new OrdStatus(OrdStatus.NEW),
                new Symbol("BTC_USD"),
                new Side(side),
                new LeavesQty(order.ActualAmount),
                new CumQty(order.OriginalAmount - order.ActualAmount),
                new AvgPx(0m)
                );

            exec_report.Set(new Price(order.Rate));
            exec_report.Set(new ClOrdID(cl_ord_id));
            exec_report.Set(new CashOrderQty(order.OriginalAmount));
            exec_report.Set(new TransactTime(order.DtMade));

            return exec_report;
        }

        private static QuickFix.FIX44.ExecutionReport FormExecReport(string cl_ord_id, bool order_kind, Order order, Trade trade)
        {
            char side = (order_kind) ? Side.SELL : Side.BUY;
            char ord_status = (order.ActualAmount == 0m) ? OrdStatus.FILLED : OrdStatus.PARTIALLY_FILLED;
            
            QuickFix.FIX44.ExecutionReport exec_report = new QuickFix.FIX44.ExecutionReport(
                new OrderID(order.OrderId.ToString()),
                new ExecID(trade.TradeId.ToString()),
                new ExecType(ExecType.TRADE),
                new OrdStatus(ord_status),
                new Symbol("BTC_USD"),
                new Side(side),
                new LeavesQty(order.ActualAmount),
                new CumQty(order.OriginalAmount - order.ActualAmount),
                new AvgPx(0m)
                );

            exec_report.Set(new Price(order.Rate));
            exec_report.Set(new ClOrdID(cl_ord_id));
            exec_report.Set(new CashOrderQty(order.OriginalAmount));
            exec_report.Set(new LastQty(trade.Amount));
            exec_report.Set(new LastPx(trade.Rate));
            exec_report.Set(new TransactTime(trade.DtMade));

            return exec_report;
        }

        private static QuickFix.FIX44.ExecutionReport FormExecReport(StatusCodes err_code, long func_call_id, QuickFix.FIX44.NewOrderSingle ord, Order order)
        {
            QuickFix.FIX44.ExecutionReport exec_report = new QuickFix.FIX44.ExecutionReport(
                new OrderID(order.OrderId.ToString()),
                new ExecID(func_call_id.ToString()),
                new ExecType(ExecType.REJECTED),
                new OrdStatus(OrdStatus.REJECTED),
                new Symbol("BTC_USD"),
                ord.Side,
                new LeavesQty(0m),
                new CumQty(0m),
                new AvgPx(0m)
                );

            exec_report.Set(new Text(err_code.ToString()));
            exec_report.Set(ord.Price);
            exec_report.Set(ord.ClOrdID);
            exec_report.Set(ord.CashOrderQty);
            exec_report.Set(new TransactTime(order.DtMade));

            return exec_report;
        }

        private static QuickFix.FIX44.ExecutionReport FormExecReport(StatusCodes status_code, QuickFix.FIX44.OrderStatusRequest stareq, bool order_kind, Order order)
        {
            char side = (order_kind) ? Side.SELL : Side.BUY;
            char exec_type;
            char ord_status;

            if (status_code == StatusCodes.Success)
            {
                exec_type = ExecType.ORDER_STATUS;
                if (order.OriginalAmount == order.ActualAmount) ord_status = OrdStatus.NEW;
                else ord_status = OrdStatus.PARTIALLY_FILLED;
            }
            else
            {
                exec_type = ExecType.REJECTED;
                ord_status = OrdStatus.REJECTED;
            }

            QuickFix.FIX44.ExecutionReport exec_report = new QuickFix.FIX44.ExecutionReport(
                new OrderID(order.OrderId.ToString()),
                new ExecID("0"),
                new ExecType(exec_type),
                new OrdStatus(ord_status),
                new Symbol("BTC_USD"),
                new Side(side),
                new LeavesQty(order.ActualAmount),
                new CumQty(order.OriginalAmount - order.ActualAmount),
                new AvgPx(0m)
                );

            exec_report.Set(stareq.ClOrdID);
             
            if (status_code == StatusCodes.Success)
            {
                exec_report.Set(new Price(order.Rate));
                exec_report.Set(new CashOrderQty(order.OriginalAmount));
                exec_report.Set(new TransactTime(order.DtMade));
            }
            else //выводим описание ошибки
            {
                exec_report.Set(new Text(status_code.ToString()));
            }           

            return exec_report;
        }

        private static QuickFix.FIX44.ExecutionReport FormExecReport(long func_call_id, QuickFix.FIX44.OrderCancelRequest canreq, bool order_kind, Order order)
        {
            char side = (order_kind) ? Side.SELL : Side.BUY;
            
            QuickFix.FIX44.ExecutionReport exec_report = new QuickFix.FIX44.ExecutionReport(
                new OrderID(order.OrderId.ToString()),
                new ExecID(func_call_id.ToString()),
                new ExecType(ExecType.CANCELED),
                new OrdStatus(OrdStatus.CANCELED),
                new Symbol("BTC_USD"),
                new Side(side),
                new LeavesQty(order.ActualAmount),
                new CumQty(order.OriginalAmount - order.ActualAmount),
                new AvgPx(0m)
                );
                        
            exec_report.Set(new Price(order.Rate));
            exec_report.Set(canreq.ClOrdID);
            exec_report.Set(new CashOrderQty(order.OriginalAmount));
            exec_report.Set(new TransactTime(order.DtMade));

            return exec_report;
        }

        private static QuickFix.FIX44.OrderCancelReject FormOrdCancReject(StatusCodes err_code, long func_call_id, QuickFix.FIX44.OrderCancelRequest canreq)
        {
            QuickFix.FIX44.OrderCancelReject ocr = new QuickFix.FIX44.OrderCancelReject(
                canreq.OrderID,
                canreq.ClOrdID,
                new OrigClOrdID("0"),
                new OrdStatus(OrdStatus.REJECTED),
                new CxlRejResponseTo(CxlRejResponseTo.ORDER_CANCEL_REQUEST)
                );

            ocr.Set(new Text(err_code.ToString()));

            return ocr;
        }

        #endregion

    }
}
