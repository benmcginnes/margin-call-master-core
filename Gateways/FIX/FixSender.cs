﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;
using QuickFix;
using QuickFix.Fields;

namespace RocketCore
{
    class FixSender
    {
        internal FixSender()
        { }

        internal void SpawnFixSenderThread(Session s)
        {
            Thread fix_sender_thread = new Thread(new ParameterizedThreadStart(HandleFixSenderThread));
            fix_sender_thread.Start(s);
            Console.WriteLine("FIX: sender thread started");
        }

        private void HandleFixSenderThread(object obj)
        {
            Session s = obj as Session;

            ConcurrentQueue<Message> queue;
            if (Queues.fix_dict.TryGetValue(s.SessionID.TargetCompID, out queue))
            {
                while (s.IsLoggedOn)
                {
                    //отправляем FIX-сообщения из соответствующей очереди
                    Message msg;
                    if (queue.TryPeek(out msg))
                    {
                        try
                        {
                            bool _sent = s.Send(msg);

                            if (_sent)
                            {
                                Console.WriteLine("FIX: message sent");
                                queue.TryDequeue(out msg);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }
                }
            }

            Console.WriteLine("FIX: exiting sender thread (session gone offline)");
        }

    }
}
